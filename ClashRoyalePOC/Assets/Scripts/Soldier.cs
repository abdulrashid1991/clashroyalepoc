﻿using UnityEngine;
using System.Collections;

public class Soldier : Attacker
{
    public float _RecoverTime;
    public MeshRenderer _Renderer;

    private Color mDefaultColor;
    private float mLastAttackTime;
    public static void SpawnAttacker(Player player, Vector3 position)
    {
        switch (player)
        {
            case Player.Home:
                ((GameObject)GameObject.Instantiate(GameController.Instance.mGameData._HomeSoldgerPrefab, position, Quaternion.identity)).transform.parent = GameController.Instance._UIManager.transform;
                break;
            case Player.Away:
                ((GameObject)GameObject.Instantiate(GameController.Instance.mGameData._AwaySoldgerPrefab, position, Quaternion.identity)).transform.parent = GameController.Instance._UIManager.transform;
                break;
            default:
                break;
        }
    }
    public override void Init()
    {
        GameController.Instance.mTargetNotfier.SearchTarget(this);
        _NavAgent.stoppingDistance = _MinAttackDistance;
        _NavAgent.speed = _Speed;
        mLastAttackTime = _AttackDelay;
        mDefaultColor = _Renderer.material.color;
        WakeUpPlayer();
    }
    void Update() 
    {
        if (mIsActive && mActiveTarget != null)
        {
            if (mActiveTarget._Type == TargetType.Moving)
            {
                UpdateDestination(mActiveTarget.transform.position);                
            }
            CalculateAttack();
        }
    }
    private void CalculateAttack()
    {
        if (Vector3.Distance(transform.position,mActiveTarget.transform.position) <= _MinAttackDistance)
        {
            if (mLastAttackTime <= 0)
            {
                AttackTarget(mActiveTarget);
                mLastAttackTime = _AttackDelay;
            }
            else
            {
                mLastAttackTime -= Time.deltaTime;
            }
        }
    }
    protected override void UpdateDestination(Vector3 position)
    {
        _NavAgent.SetDestination(position);
    }
    protected override void AttackTarget(ILife target)
    {
        _Renderer.material.color = Color.black;
        target.HitRecived(_HitPoint);
        Invoke("Reset", _RecoverTime);
    }
    public override void TargetAcquired(Target target)
    {
        mActiveTarget = target;
        mActiveTarget.mRefreshTargetCallBack = OnRefreshTarget;
        UpdateDestination(target.transform.position);
    }
    protected override void OnRefreshTarget()
    {
        mActiveTarget = null;
        GameController.Instance.mTargetNotfier.SearchTarget(this);
    }

    protected override void OnHitRecived()
    {
        _Renderer.material.color = Color.red;
        Invoke("Reset", _RecoverTime);
    }

    protected virtual void Reset()
    {
        _Renderer.material.color = mDefaultColor;
    }


}
