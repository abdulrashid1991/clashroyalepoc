﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour , IDestroyed,IObservable<IGameOver>
{
    public static GameController Instance 
    {
        get
        {
            if (mInstance == null)
            {
                mInstance = new GameObject("GameController").AddComponent<GameController>();
                DontDestroyOnLoad(mInstance.gameObject);
            }
            return mInstance;
        }
    }
    private static GameController mInstance;
    public GameData mGameData;
    public UIManager _UIManager;
    public ITargetStateNotifier mTargetNotfier;

    private List<IGameOver> mListners;
    private TargetManager mTargetManager;
    private float mLastRefreshTime;
    private bool mIsGameOver = false;
    private SpawnManager mHomeSpawnManager,mAwaySpawnManager;
    private GameController() { }
    void Awake()
    {        
        GameObject.DontDestroyOnLoad(this);
        Init();
    }
    public void Init()
    {
        mTargetManager = gameObject.AddComponent<TargetManager>();
        mTargetNotfier = mTargetManager;
        mGameData = GameData.LoadData();
        mLastRefreshTime = mGameData._RefreshDelay;
        mHomeSpawnManager = new SpawnManager(mGameData._SpawnConfig,Player.Home);
        mAwaySpawnManager = new SpawnManager(mGameData._SpawnConfig, Player.Away);
        mListners = new List<IGameOver>();
    }

    void Start()
    {
        StartCoroutine(SpawnPlayers());
    }
    void Update()
    {
        if (mIsGameOver)
        {
            return;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            InitSoldier(Player.Away);
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            InitSoldier(Player.Home);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            InitSoldier(Player.Away);
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            InitSoldier(Player.Home);
        }
        if (mLastRefreshTime < 0)
        {
            StartCoroutine(mTargetManager.RefreshTargets(TargetType.Moving));
            mLastRefreshTime = mGameData._RefreshDelay;
        }
        else
        {
            mLastRefreshTime -= Time.deltaTime;
        }
    }

    IEnumerator SpawnPlayers()
    {
        yield return new WaitForSeconds(1);
        while (!mIsGameOver)
	    {
            mAwaySpawnManager.SpawnPlayers();
            mHomeSpawnManager.SpawnPlayers();
            yield return null;
	    }
        yield break;
    }
    public void InitSoldier(Player player)
    {
        switch (player)
        {
            case Player.Home:
                Soldier.SpawnAttacker(Player.Home, mGameData._HomeSpawnPt.transform.position);
                break;
            case Player.Away:
                Soldier.SpawnAttacker(Player.Away, mGameData._AwaySpawnPt.transform.position);
                break;
        }
    }

    public void InitHero(Player player)
    {
        switch (player)
        {
            case Player.Home:
                Hero.SpawnAttacker(Player.Home, mGameData._HomeSpawnPt.transform.position);
                break;
            case Player.Away:
                Hero.SpawnAttacker(Player.Away, mGameData._AwaySpawnPt.transform.position);
                break;
        }
    }

    public void OnDestroyed(BuilidingType building, Player player)
    {
        if (building == BuilidingType.Base)
        {
            mIsGameOver = true;
            StartCoroutine((ClearPlayField(player)));
        }
    }
        
    public void AddObserver(IGameOver listner)
    {
        if (!mListners.Contains(listner))
        {
            mListners.Add(listner);
        }
    }

    public void RemoveObserver(IGameOver listner)
    {
        if (mListners.Contains(listner))
        {
            mListners.Remove(listner);
        }
    }

    private IEnumerator ClearPlayField(Player player)
    {
        while (mListners.Count > 0)
        {
            for (int i = 0; i < mListners.Count; i++)
            {
                mListners[i].OnGameCompleted();
            }
            yield return null;
        }
        _UIManager.SetWinner(player);
    }
}

