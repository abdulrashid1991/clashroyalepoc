﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    public Image _Renderer;

    private float? mFullHealth;
    private float mCurrHealth;
    private float mCutOut;

    void Start()
    {
        _Renderer.material = new Material(GameController.Instance.mGameData._UIMaterial);
    }

    public void OnHealthUpdate(float health)
    {

        if (mFullHealth == null)
	    {
            mFullHealth = health;
	    }
        mCurrHealth = health;
        if (mCurrHealth > 0 && mFullHealth.HasValue)
        {
            mCutOut = health / mFullHealth.Value;
            _Renderer.material.SetFloat("_Cutoff", 1 - mCutOut);
        }

    }

}
