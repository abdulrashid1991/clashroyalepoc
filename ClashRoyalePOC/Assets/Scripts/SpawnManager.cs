﻿using UnityEngine;
using System.Collections;

public class SpawnManager 
{
    private SpawnManagerConfig mConfig;
    private Player mThisPlayer;
    private float mLastHeroSpawnTime;
    private float mLastSoldierSpawnTime;

    private float mHeroSpawnTime 
    {
        get
        {
            return Random.Range(mConfig._HeroSpawnTime.Min, mConfig._HeroSpawnTime.Max);
        }
    }

    private float mSoldierSpawnTime
    {
        get
        {
            return Random.Range(mConfig._SoldierSpawnTime.Min, mConfig._SoldierSpawnTime.Max);
        }
    }

    private int mSoldierSpawnCount
    {
        get
        {
            return Random.Range(mConfig._SoldierSpawnCount.Min, mConfig._SoldierSpawnCount.Max);
        }
    }

    private int mHeroSpawnCount
    {
        get
        {
            return Random.Range(mConfig._HeroSpawnCount.Min, mConfig._HeroSpawnCount.Max);
        }
    }

    public SpawnManager(SpawnManagerConfig config, Player player)
    {
        mConfig = config;
        mThisPlayer = player;
        mLastHeroSpawnTime = mHeroSpawnTime;

    }

    public void SpawnPlayers()
    {
        if (mLastHeroSpawnTime <= 0)
        {
            for (int i = 0; i < mHeroSpawnCount; i++)
            {
                GameController.Instance.InitHero(mThisPlayer);
            }
            mLastHeroSpawnTime = mHeroSpawnTime;
        }
        else
        {
            mLastHeroSpawnTime -= Time.fixedDeltaTime;
        }

        if (mLastSoldierSpawnTime <= 0)
        {
            for (int i = 0; i < mSoldierSpawnCount; i++)
            {
                GameController.Instance.InitSoldier(mThisPlayer);
            }
            mLastSoldierSpawnTime = mSoldierSpawnTime;
        }
        else
        {
            mLastSoldierSpawnTime -= Time.fixedDeltaTime;
        }
    }
    

}
