﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour 
{
    public Text _ScoreText;

    void Awake()
    {
        GameController.Instance._UIManager = this;

    }

    void Start()
    {
        _ScoreText.text = "";
    }

    public void SetWinner(Player winner)
    {
        Debug.LogError(winner.ToString() + " Won The Game");
        _ScoreText.text = winner.ToString() + " Won The Game";
    }

    public void InitHomeHero()
    {
        GameController.Instance.InitHero(Player.Home);
    }
    public void HomeSoldier()
    {
        GameController.Instance.InitSoldier(Player.Home);
    }
    public void InitAwayHero()
    {
        GameController.Instance.InitHero(Player.Away);
    }
    public void InitAwaySoldier()
    {
        GameController.Instance.InitSoldier(Player.Away);
    }
}
