﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameData : ScriptableObject
{
    private const string AssetName = "GameData";
    [Header("Prefabs")]
    public GameObject _HomeSoldgerPrefab;
    public GameObject _AwaySoldgerPrefab;
    public GameObject _HomeHeroPrefab;
    public GameObject _AwayHeroPrefab;
    public GameObject _HomeSpawnPt;
    public GameObject _AwaySpawnPt;

    [Header("GameSettings")]
    public float _RefreshDelay;
    public Material _UIMaterial;

    [Header("Spaen Manager Serttings")]
    public SpawnManagerConfig _SpawnConfig;

    public static GameData LoadData()
    {
        return Resources.Load<GameData>(AssetName);
    }
}


[System.Serializable]
public struct SpawnTime
{
    public float Min;
    public float Max;
}

[System.Serializable]
public struct SpawnCount
{
    public int Min;
    public int Max;
}
[System.Serializable]
public struct SpawnManagerConfig
{
    public SpawnTime _HeroSpawnTime;
    public SpawnTime _SoldierSpawnTime;
    public SpawnCount _HeroSpawnCount;
    public SpawnCount _SoldierSpawnCount;
}