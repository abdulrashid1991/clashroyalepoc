﻿using UnityEngine;
using System.Collections;


public abstract class Target : MonoBehaviour,ILife ,ITargetFoundNotifier
{
    public float _Health;
    public TargetType _Type;
    public HealthNotifyEvent _HealthNotifier;
    public Player _Player;

    [HideInInspector]
    public int mTargetId;
    [HideInInspector]
    public OnRefreshTarget mRefreshTargetCallBack;

    protected bool mIsActive;
    protected Target mActiveTarget;

    private static int mTargetCount = 0;

    void Awake()
    {
        mTargetId = mTargetCount++;
    }
    void Start()
    {
        Init();
        GameController.Instance.mTargetNotfier.AddNewTarget(this);
    }
    public void HitRecived(float attackPower)
    {
        OnHitRecived();
        _Health -= attackPower;
        _HealthNotifier.Invoke(_Health);
        if (_Health <= 0)
        {
            DestroyMe();
        }
    }
    public void BoostRecived(float boostPower)
    {
        _Health += boostPower;
        _HealthNotifier.Invoke(_Health);
    }
    public virtual void Init() { }

    protected virtual void OnHitRecived() { }
    public abstract void TargetAcquired(Target target);
    protected abstract void OnRefreshTarget();
    public bool RefreshTarget()
    {
        OnRefreshTarget();
        return true;
    }
    public virtual void DestroyMe()
    {
        GameController.Instance.mTargetNotfier.RemoveTarget(this);
        if (mRefreshTargetCallBack != null)
        {
            mRefreshTargetCallBack.Invoke();
        }
        mRefreshTargetCallBack = null;
        Destroy(this.gameObject);
    }
}
