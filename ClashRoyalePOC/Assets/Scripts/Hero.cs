﻿using UnityEngine;
using System.Collections;

public class Hero : Soldier 
{
    public static void SpawnAttacker(Player player, Vector3 position)
    {
        switch (player)
        {
            case Player.Home:
                ((GameObject)GameObject.Instantiate(GameController.Instance.mGameData._HomeHeroPrefab, position, Quaternion.identity)).transform.parent = GameController.Instance._UIManager.transform;
                break;
            case Player.Away:
                ((GameObject)GameObject.Instantiate(GameController.Instance.mGameData._AwayHeroPrefab, position, Quaternion.identity)).transform.parent = GameController.Instance._UIManager.transform;
                break;
            default:
                break;
        }
    }
    protected override void AttackTarget(ILife target)
    {
        base.AttackTarget(target);
    }
}
