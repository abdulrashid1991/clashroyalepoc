﻿using UnityEngine;
using System.Collections;


public class Defender : Target {

    public float _Range;
    public int _HitPoint;
    public float _ReloadDelay;
    public BuilidingType _BuildingType;

    private IDestroyed mDestroyedCallBack;
    public override void Init()
    {
        mDestroyedCallBack = GameController.Instance;        
    }
    public override void DestroyMe()
    {
        mDestroyedCallBack.OnDestroyed(_BuildingType, _Player);
        base.DestroyMe();
    }

    public override void TargetAcquired(Target target)
    {
        //TODO: Implement Defender
    }

    protected override void OnRefreshTarget()
    {
        //TODO: Implement Defender
    }
}
